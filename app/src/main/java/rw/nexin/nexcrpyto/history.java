package rw.nexin.nexcrpyto;

public class history {

    private String receiver,amount,date;

    public history(){

    }
    public history(String receiver, String amount, String date){
        this.receiver = receiver;
        this.amount = amount;
        this.date = date;
    }

    public  String getReceiver(){
        return receiver;
    }

    public void setReceiver(String name){
        this.receiver = name;
    }

    public String getDate(){
        return date;
    }

    public void setDate(String date){
        this.date = date;
    }

    public String getAmount(){
        return amount;
    }

    public void setAmount(String amount){
        this.amount = amount;
    }

}

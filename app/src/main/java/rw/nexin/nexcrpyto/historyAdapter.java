package rw.nexin.nexcrpyto;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class historyAdapter extends RecyclerView.Adapter<historyAdapter.MyViewHolder> {

    private List<history> sentList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView Receiver, Amount,Date;

        public MyViewHolder(View view){
            super(view);

            Receiver = (TextView) view.findViewById(R.id.receiver);
            Amount = (TextView) view.findViewById(R.id.amount);
            Date = (TextView) view.findViewById(R.id.date);
        }
    }

    public historyAdapter(List<history> sentList){
        this.sentList = sentList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sent,parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        history hist = sentList.get(position);
        holder.Receiver.setText(hist.getReceiver());
        holder.Amount.setText(hist.getAmount());
        holder.Date.setText(hist.getDate());
    }

    @Override
    public int getItemCount(){
        return sentList.size();
    }
}

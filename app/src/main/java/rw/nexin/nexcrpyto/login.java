package rw.nexin.nexcrpyto;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class login extends AppCompatActivity {

    private EditText email;
    private EditText password;
    private Button loginButton;
    private Context context;
    private SharedPreferences user_data;
    private ProgressBar loginProgress;
    private TextView signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        user_data = getSharedPreferences(getString(R.string.user_data), MODE_PRIVATE);
        email = (EditText) findViewById(R.id.loginEmail);
        password = (EditText) findViewById(R.id.loginPassword);
        loginButton = (Button) findViewById(R.id.loginButton);
        loginProgress = (ProgressBar) findViewById(R.id.loginProgress);
        signUp = (TextView) findViewById(R.id.signUp);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,signUp.class);
                startActivity(intent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_email = email.getText().toString();
                String user_password = password.getText().toString();

                if (user_email.isEmpty() || user_email == null){
                    email.setError("This Field is Required");
                }else if(user_password.isEmpty() || user_password == null){
                    password.setError("This Field is required");
                }else{
                    loginProgress.setVisibility(View.VISIBLE);
                    performLogin(user_email,user_password);
                }
            }
        });
    }

    public void performLogin(final String user_email, final String user_password){
        StringRequest request = new StringRequest(Request.Method.POST, "https://nex-pay.herokuapp.com/login/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loginProgress.setVisibility(View.GONE);
                try{

                   JSONObject data = new JSONObject(response);
                   user_data.edit()
                           .putBoolean(getString(R.string.is_logged_in),true)
                           .putString("id",data.getString("id"))
                           .putString("names",data.getString("names"))
                           .putString("email",data.getString("email"))
                           .putString("phone",data.getString("phone"))
                           .putString("publicKey",data.getString("publicKey"))
                           .apply();

                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                    finish();

                } catch (JSONException e){
                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loginProgress.setVisibility(View.GONE);
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("phone",user_email);
                params.put("password",user_password);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }
}

package rw.nexin.nexcrpyto;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SharedPreferences user_data;
    private Context context;
    String email,username,userId,phoneNbr;
    private TextView balanceView,usdbalanceView,publicKeyView;
    private SharedPreferences pref;
    private SwipeRefreshLayout swipeRefresh;
    private String ENTRIES_DIR;
    private String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Nex Pay");

        context = this;
        user_data = getSharedPreferences(getString(R.string.user_data),MODE_PRIVATE);
        pref = getSharedPreferences(getString(R.string.pref_user),MODE_PRIVATE);
        swipeRefresh = (SwipeRefreshLayout)findViewById(R.id.swipeRefresh);
        balanceView = (TextView) findViewById(R.id.balance);
        usdbalanceView = (TextView) findViewById(R.id.usdBalance);
        publicKeyView = (TextView) findViewById(R.id.publicKey);

        Boolean isLogged = user_data.getBoolean(getString(R.string.is_logged_in),false);

        if(isLogged == false){
            Intent intent = new Intent(context,login.class);
            startActivity(intent);
            finish();
        }else{

            email = user_data.getString("email","- - - - -");
            username = user_data.getString("names","- - - - - -");
            userId = user_data.getString("id","- - - - - - -");
            phoneNbr = user_data.getString("phone","- - - - - - -");
            key = user_data.getString("publicKey","- - - - - - - ");
            //loadData(userId);
            setupStellarAccount(key);
            setUpTextView();
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,send.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);

        TextView userName = (TextView) header.findViewById(R.id.textView1);
        TextView userMail = (TextView) header.findViewById(R.id.textView2);

        userName.setText(username);
        userMail.setText(email);

        //setUpTextView();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.balance:
                        balanceView.setVisibility(View.GONE);
                        usdbalanceView.setVisibility(View.VISIBLE);
                        break;
                    case R.id.usdBalance:
                        usdbalanceView.setVisibility(View.GONE);
                        balanceView.setVisibility(View.VISIBLE);
                        break;
                }
            }
        };

        balanceView.setOnClickListener(listener);
        usdbalanceView.setOnClickListener(listener);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setupStellarAccount(key);
                setUpTextView();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                    }
                }, 2000);
            }
        });
    }
    public void setUpTextView(){
        //Method to setup TextViews

        Float ActualBalance = Float.parseFloat(pref.getString("balance","0.0"));
        Float CurrentRate = Float.parseFloat(pref.getString("currentRate","0.0"));
        Float usdAmount = ActualBalance * CurrentRate;

        balanceView.setText(pref.getString("balance","- - - - - - - - - - -")+" XLM ");
        publicKeyView.setText(user_data.getString("publicKey","- - - - - - - - - - - - - - - "));
        usdbalanceView.setText(usdAmount.toString()+" USD");

    }

    public void setupStellarAccount(final String key){
        String url = "https://horizon-testnet.stellar.org/accounts/" + key;

        if (key == null || key.isEmpty()){
            url = "https://horizon-testnet.stellar.org/accounts/GBQL6ZN6PH5Z4V3SUNIUZ4SA6SS3M4O63DMZOFDORIMATYYLNOGTYFY3";
        }

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject data = new JSONObject(response);

                    JSONArray balances = data.getJSONArray("balances");
                    JSONObject balance = balances.getJSONObject(0);
                    String currentBalance = balance.getString("balance");

                    Float actualBalance = Float.parseFloat(currentBalance);

                    pref.edit()
                            .putString("balance", currentBalance)
                            .apply();
                    //balanceView.setText(pref.getString("balance","- - - - - - - - - - -"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Check Your internet", Toast.LENGTH_SHORT).show();
            }
        });

        //Lumens current rate
        String xlm_url = "https://api.cryptonator.com/api/full/xlm-usd";
        StringRequest xlmrequest = new StringRequest(Request.Method.GET, xlm_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject exchangeRate = new JSONObject(response);
                    JSONObject ticker = exchangeRate.getJSONObject("ticker");
                    String currentRate = ticker.getString("price");
                    pref.edit()
                            .putString("currentRate", currentRate)
                            .apply();

                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
        queue.add(xlmrequest);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(context,SettingsActivity.class);
            startActivity(intent);
            return true;
        }else if(id == R.id.action_about){
            Toast.makeText(context, "Clicked About", Toast.LENGTH_SHORT).show();
        }else if(id == R.id.action_faq){
            Toast.makeText(context, "Clicked Faq", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_manage) {
            Toast.makeText(context, "Clicked Settings", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(context,SettingsActivity.class);
            startActivity(intent);

        }else if(id == R.id.send){

            Toast.makeText(this, "Clicked send", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(context,send.class);
            startActivity(intent);
        }
        else if (id == R.id.logout) {

            user_data.edit()
                    .putBoolean(getString(R.string.is_logged_in),false)
                    .apply();
            Intent intent = new Intent(this,login.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.transactions) {
            Intent intent = new Intent(context,transactions.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

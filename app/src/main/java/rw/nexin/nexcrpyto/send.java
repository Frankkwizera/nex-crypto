package rw.nexin.nexcrpyto;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Integer.parseInt;


public class send extends AppCompatActivity {

    private Context context;
    private TextView sendBalance;
    private SharedPreferences pref;
    private SharedPreferences user_data;
    private ProgressBar sendProgressBar;
    private ImageView imageView;
    private Button barcodeCamera;
    private Bitmap bitmap;
    private EditText sendAmount,receiverPhone;
    private String publicKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);

        context = this;
        final Activity activity = this;
        user_data = getSharedPreferences(getString(R.string.user_data), MODE_PRIVATE);
        pref = getSharedPreferences(getString(R.string.pref_user),MODE_PRIVATE);
        Button transfer = (Button) findViewById(R.id.transfer);
        sendBalance = (TextView) findViewById(R.id.sendBalance);
        sendProgressBar = (ProgressBar) findViewById(R.id.sendProgressBar);
        imageView = (ImageView) findViewById(R.id.receiveBarCode);
        barcodeCamera = (Button) findViewById(R.id.barcodeCamera);
        sendAmount = (EditText) findViewById(R.id.sendAmount);
        receiverPhone = (EditText) findViewById(R.id.receiverPhone);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Send");
        setupActionBar();

        final String availableBalance = pref.getString("balance","- - - - - - - - -");
        sendBalance.setText("Balance "+availableBalance);

        publicKey = user_data.getString("publicKey","No key supplied in Settings");

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {

            BitMatrix bitMatrix = multiFormatWriter.encode(publicKey,BarcodeFormat.QR_CODE,300,300);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            bitmap = barcodeEncoder.createBitmap(bitMatrix);
            imageView.setImageBitmap(bitmap);

        }catch (WriterException e){
            e.printStackTrace();
        }

        sendAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Toast.makeText(activity, "Before being changed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Toast.makeText(activity, "Currently being changed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable s) {
                String amount = sendAmount.getText().toString();
                if (amount == null || amount.isEmpty()){
                    sendBalance.setText("Balance "+availableBalance);
                }else {
                    Float sendingAmount = Float.parseFloat(amount);
                    Float availableAmount = Float.parseFloat(availableBalance);

                    Float remainBalance = availableAmount - sendingAmount;

                    sendBalance.setText("Balance " + String.valueOf(remainBalance));
                }
            }
        });

        transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 String receiverAddr = receiverPhone.getText().toString();
                 String transferAmount = sendAmount.getText().toString();
                 String text = "Debt settlement";

                 if (receiverAddr.isEmpty() || receiverAddr == null){
                     Snackbar.make(v, "Receiver address is needed or scan his/her barcode", Snackbar.LENGTH_LONG)
                             .setAction("Action", null).show();
                 }else if(transferAmount.isEmpty() || transferAmount == null){
                     sendAmount.setError("Enter valid amount to send");
                 }else {
                     sendProgressBar.setVisibility(View.VISIBLE);
                     makePayment(publicKey,receiverAddr,transferAmount,text);
                 }
            }
        });

        barcodeCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(activity);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.setPrompt("Scanning Nex Pay ID");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();
            }
        });
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);

        }
    }

    public void makePayment(final String senderAddr,final String receiverAddr, final String sentAmount, final String text){

        Map<String, String> paramsToJson = new HashMap<>();
        paramsToJson.put("From",senderAddr);
        paramsToJson.put("To",receiverAddr);
        paramsToJson.put("Amount",sentAmount);
        paramsToJson.put("Text",text);

        JSONObject params = new JSONObject(paramsToJson);

        StringRequest request = new StringRequest(Request.Method.POST, "https://nex-pay.herokuapp.com/api/v1/payments/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(context, "Sent successfully", Toast.LENGTH_SHORT).show();
                sendProgressBar.setVisibility(View.GONE);
                Intent intent = new Intent(context,MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sendProgressBar.setVisibility(View.GONE);
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }){
          @Override
          protected Map<String,String> getParams(){
              Map<String,String> params = new HashMap<>();
              params.put("From",senderAddr);
              params.put("To",receiverAddr);
              params.put("Amount",sentAmount);
              params.put("Text",text);

              return params;
          }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if (result != null){
            if (result.getContents() == null ){
                Toast.makeText(context, "You cancelled Scanning ", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(context, result.getContents(), Toast.LENGTH_SHORT).show();
                receiverPhone.setText(result.getContents());
            }
        }else {

            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}

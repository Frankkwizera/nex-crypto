package rw.nexin.nexcrpyto;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class signUp extends AppCompatActivity {
    private EditText username,email,password,RePassword,phoneNbr;
    private Button signUp;
    private ProgressBar signUpProgress;
    private Context context;
    private SharedPreferences user_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        context = this;
        username = (EditText) findViewById(R.id.signUpusername);
        email = (EditText) findViewById(R.id.signUpEmail);
        password = (EditText) findViewById(R.id.signUpPassword);
        RePassword = (EditText) findViewById(R.id.signUpRePassword);
        phoneNbr = (EditText) findViewById(R.id.signUpPhoneNbr);
        signUp = (Button) findViewById(R.id.signUpBtn);
        signUpProgress = (ProgressBar) findViewById(R.id.signUpProgess);
        user_data = getSharedPreferences(getString(R.string.user_data), MODE_PRIVATE);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_username = username.getText().toString();
                String user_email = email.getText().toString();
                String user_password = password.getText().toString();
                String user_RePassword = RePassword.getText().toString();
                String user_phoneNbr = phoneNbr.getText().toString();
                if(user_username.isEmpty() || user_username == null){
                    username.setError("Username is Required");
                }else if(user_email.isEmpty() || user_email == null){
                    email.setError("Email is Required");
                }else if(user_password.isEmpty() || user_password == null){
                    password.setError("Password is Required");
                }else if(user_phoneNbr.isEmpty() || user_phoneNbr == null){
                    phoneNbr.setError("Phone nbr is Required");
                }
                else if(user_RePassword.isEmpty() || user_RePassword == null){
                    RePassword.setError(" Repeat Password ");
                }else if(user_password.intern() != user_RePassword.intern()){
                    RePassword.setError("Password didn't match");
                }else {
                    signUpProgress.setVisibility(View.VISIBLE);
                    createUser(user_email,user_password,user_username,user_phoneNbr);
                }
            }
        });
    }

    public void createUser(final String user_email,final String user_password, final String user_username, final String user_phoneNbr){

        StringRequest request = new StringRequest(Request.Method.POST, "https://nex-pay.herokuapp.com/api/v1/users/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                signUpProgress.setVisibility(View.GONE);
                try{

                    JSONObject data = new JSONObject(response);
                    user_data.edit()
                            .putBoolean(getString(R.string.is_logged_in),true)
                            .putString("id",data.getString("id"))
                            .putString("names",data.getString("names"))
                            .putString("email",data.getString("email"))
                            .putString("phone",data.getString("phone"))
                            .putString("publicKey",data.getString("publicKey"))
                            .apply();

                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                    finish();

                } catch (JSONException e){
                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("names", user_username);
                params.put("email", user_email);
                params.put("phone", user_phoneNbr);
                params.put("password", user_password);

                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }
}
